﻿using System;
using System.Text.RegularExpressions;

namespace TextReader
{
    public class TextOutputWriter
    {
        private int SentenceIndex = 0;
        private int SymbolIndex = 0;
        private string Text;
        private MatchCollection Sentences;
        private MatchCollection Symbols;

        public TextOutputWriter (string text)
        {
            this.Text = text;
            this.AnalyzeSentences();
            this.AnalyzeSymbols();
        }

        private void AnalyzeSentences ()
        {
            this.Sentences = Regex.Matches(this.Text, "[^.?!]+[.?!]");
        }

        private void AnalyzeSymbols ()
        {
            string sentence = this.Sentences[this.SentenceIndex].Value;
            this.Symbols = Regex.Matches(sentence, "[^\\w\\s]");
        }

        private void NextSentence ()
        {
            if (this.SentenceIndex + 1 >= this.Sentences.Count)
            {
                this.SentenceIndex = 0;
                return;
            }
            this.SentenceIndex++;
        }

        public void Next()
        {
            if(this.SymbolIndex + 1 >= this.Symbols.Count)
            {
                this.SymbolIndex = 0;
                this.NextSentence();
                this.AnalyzeSymbols();
                return;
            }
            this.SymbolIndex++;
        }

        public void Print ()
        {
            Match sentence = this.Sentences[this.SentenceIndex];
            Match symbol = this.Symbols[this.SymbolIndex];
            Console.SetCursorPosition(0, 0);

            Console.Write(this.Text.Substring(0, sentence.Index));

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write(sentence.Value.Substring(0, symbol.Index));
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(symbol.Value);
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write(sentence.Value.Substring(symbol.Index + symbol.Length));

            Console.ResetColor();

            Console.Write(this.Text.Substring(sentence.Index + sentence.Length));
        }
    }
}
