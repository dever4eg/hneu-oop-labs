﻿using System;
using System.IO;

namespace TextReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = File.ReadAllText("/Users/dever/dev/dotnet/LabsHNEU/TextReader/input.txt");

            TextOutputWriter writer = new TextOutputWriter(text);

            writer.Print();
            
            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                writer.Next();
                writer.Print();
            }
        }
    }
}
