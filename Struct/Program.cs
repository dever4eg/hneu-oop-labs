﻿using System;

namespace Struct
{
    struct Train
    {
        public string Destination;
        public int Number;
        public DateTime DepartureTime;

        public Train(string Destination, int Number, DateTime DepartureTime)
        {
            this.Destination = Destination;
            this.Number = Number;
            this.DepartureTime = DepartureTime;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            const int n = 1;

            Train[] trains = new Train[n];

            for(int i = 0; i < n; i++)
            {
                Train t = new Train();
                Console.WriteLine("Create train " + (i + 1));

                Console.Write("Enter train number: ");
                t.Number = int.Parse(Console.ReadLine());

                Console.Write("Enter train destination: ");
                t.Destination = Console.ReadLine();

                Console.Write("Enter train departure time (example: 05/29/2015 05:50 AM): ");
                t.DepartureTime = DateTime.Parse(Console.ReadLine());

                trains[0] = t;
            }

            Console.Write("Find train by number: ");
            int number = int.Parse(Console.ReadLine());

            if (Array.Exists(trains, train => train.Number == number))
            {
                Train train = Array.Find(trains, train => train.Number == number);
                Console.WriteLine(
                    "Train №{0}, destination: {1}, departure time: {2}",
                    train.Number, train.Destination, train.DepartureTime
                );
            }
            else Console.WriteLine("Train not found");
        }
    }
}
