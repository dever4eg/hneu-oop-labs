﻿using System;
using System.Text.RegularExpressions;

namespace LabsHNEU.Lab2
{
    public class ConstExpression
    {
        public int Parse(string input)
        {
            input = input.Replace(" ", "");
            int arg1, arg2, i = 0;

            Match number = Regex.Match(input, "^\\d+");
            if (number.Success)
            {
                arg1 = int.Parse(number.Value);
                i = number.Length - 1;
            }
            else if (input[0] == '(')
            {
                for (int n = 0; i < input.Length - 1; i++)
                {
                    if (input[i] == '(') n++;
                    if (input[i] == ')') n--;
                    if (n == 0) break;
                }
                arg1 = this.Parse(input[1..i]);
            }
            else throw new Exception("Invalid expression");

            if (input.Length == i + 1) return arg1;

            char op = input[++i];

            arg2 = this.Parse(input.Substring(++i));

            return op switch
            {
                '+' => arg1 + arg2,
                '-' => arg1 - arg2,
                '*' => arg1 * arg2,
                _ => throw new Exception("Unsupported operation given: " + op),
            };
        }
    }
}
