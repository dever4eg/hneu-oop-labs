﻿using System;
using System.Text.RegularExpressions;

namespace LabsHNEU.Lab2
{
    public class WordCountFilter
    {
        public string Filter (string text, int wordCount)
        {
            string result = "";

            MatchCollection sentences = Regex.Matches(text, "[^.?!]+[.?!]");

            foreach(Match sentence in sentences)
            {
                string[] words = Regex.Split(sentence.Value, "\\s+");
                if (words.Length == wordCount)
                {
                    result += sentence.Value;
                }
            }

            return result;
        }
    }
}
