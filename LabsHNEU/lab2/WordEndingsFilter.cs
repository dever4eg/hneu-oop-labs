﻿using System;
using System.Text.RegularExpressions;

namespace LabsHNEU.Lab2
{
    public class WordEndingsFilter
    {
        public string filter (string str, string[] endings)
        {
            String regex = "[ ]*[А-Яа-я\\w]+";
            regex = regex + '(' + String.Join("|", endings) + ')';

            String result = Regex.Replace(str, regex, "").Trim();
            result = result.Substring(0, 1).ToUpper() + result.Substring(1);

            return result;
        }
    }
}
