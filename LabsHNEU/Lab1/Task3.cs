﻿using System;
namespace LabsHNEU.Lab1
{
    public class Task3
    {
        public float FindMaxAbs(float[] numbers)
        {
            float max = 0;
            foreach (float number in numbers)
            {
                float abs = Math.Abs(number);
                max = max < abs ? abs : max;
            }
            return max;

        }

        public float SumBetweenFirstTwoPositive(float[] numbers)
        {
            bool computing = false;
            float sum = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (!computing)
                {
                    computing = numbers[i] > 0;
                    continue;
                }
                if (numbers[i] > 0) break;
                sum += numbers[i];
            }

            return sum;
        }

        public float[] MoveZeroToEnd(float[] numbers)
        {
            int notZeroCount = 0;

            for (int i = 0; i < numbers.Length; i++)
                if (numbers[i] != 0)
                    numbers[notZeroCount++] = numbers[i];

            while (notZeroCount < numbers.Length)
                numbers[notZeroCount++] = 0;

            return numbers;
        }
    }
}
