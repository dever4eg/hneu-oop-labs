﻿using System;
namespace LabsHNEU.Lab1
{
    public class Task4Matrix
    {
        public int MatrixLocalMinimumCount(float[,] matrix)
        {
            int width = matrix.GetLength(0);
            int height = matrix.GetLength(1);

            int count = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (
                        (i == 0          || matrix[i, j] < matrix[i - 1, j]) &&
                        (i == width - 1  || matrix[i, j] < matrix[i + 1, j]) &&
                        (j == 0          || matrix[i, j] < matrix[i, j - 1]) &&
                        (j == height - 1 || matrix[i, j] < matrix[i, j + 1])
                    )
                    {
                        count++;
                    }
                }
            }

            return count;
        }
    }
}
