﻿using System;
namespace LabsHNEU.Lab3
{
    public class DateHelper
    {
        public DateTime Date;

        public bool IsLeapYear
        {
            get { return DateTime.IsLeapYear(this.Date.Year); }
        }

        public DateHelper()
        {
            this.Date = new DateTime(2009, 1, 1);
        }

        public DateHelper(DateTime date)
        {
            this.Date = date;
        }

        public DateTime Yesterday ()
        {
            return this.Date.AddDays(-1);
        }

        public DateTime Tomorrow()
        {
            return this.Date.AddDays(1);
        }

        public int DaysToMonthEnd()
        {
            return DateTime.DaysInMonth(this.Date.Year, this.Date.Month) - this.Date.Day;
        }
    }
}
