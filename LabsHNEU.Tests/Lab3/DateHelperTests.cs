﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab3;

namespace LabsHNEU.Tests.Lab3
{
    [TestClass]
    public class DateHelperTests
    {
        [TestMethod]
        public void TestYesterday()
        {
            DateHelper helper = new DateHelper(new DateTime(2019, 1, 5));
            DateTime d = helper.Yesterday();
            Assert.AreEqual(4, d.Day);
        }

        [TestMethod]
        public void TestTomorrow()
        {
            DateHelper helper = new DateHelper(new DateTime(2019, 1, 5));
            DateTime d = helper.Tomorrow();
            Assert.AreEqual(6, d.Day);
        }

        [TestMethod]
        public void TestDaysInMontch()
        {
            DateHelper helper = new DateHelper(new DateTime(2019, 1, 5));
            int n = helper.DaysToMonthEnd();
            Assert.AreEqual(26, n);
        }

        [TestMethod]
        public void TestLeapYear()
        {
            DateHelper helper = new DateHelper(new DateTime(2019, 1, 5));
            Assert.AreEqual(false, helper.IsLeapYear);
        }
    }
}
