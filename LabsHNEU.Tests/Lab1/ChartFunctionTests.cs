﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab1;

namespace LabsHNEU.Tests.Lab1
{
    [TestClass]
    public class ChartFunctionTests
    {
        [TestMethod]
        public void TestInvoke()
        {
            ChartFunction fn = new ChartFunction();

            Assert.AreEqual(0, fn.Invoke(-10));
            Assert.AreEqual(0, fn.Invoke(-7));
            Assert.AreEqual(2, fn.Invoke(-5));
            Assert.AreEqual(4, fn.Invoke(-3));
            Assert.AreEqual(4, fn.Invoke(-2));
            Assert.AreEqual(1, fn.Invoke(-1));
            Assert.AreEqual(0, fn.Invoke(0));
            Assert.AreEqual(1, fn.Invoke(1));
            Assert.AreEqual(2.25f, fn.Invoke(1.5f));
            Assert.AreEqual(4, fn.Invoke(2));
            Assert.AreEqual(2, fn.Invoke(3));
            Assert.AreEqual(0, fn.Invoke(4));
            Assert.AreEqual(0, fn.Invoke(8));
        }
    }
}
