﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab1;

namespace LabsHNEU.Tests.Lab1
{
    [TestClass]
    public class Task4Tests
    {
        [TestMethod]
        public void TestLocalMinimum()
        {
            float[,] matrix = {
                { 1, 4, 6, 3 },
                { 3, 1, 3, 9 },
                { 2, 5, 9, 11},
                { 3, 4, 1, 3 }
            };

            Task4Matrix task = new Task4Matrix();

            Assert.AreEqual(5, task.MatrixLocalMinimumCount(matrix));
        }
    }
}
