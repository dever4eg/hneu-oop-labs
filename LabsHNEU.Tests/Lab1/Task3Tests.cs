﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab1;

namespace LabsHNEU.Tests.Lab1
{
    [TestClass]
    public class Task3Tests
    {
        [TestMethod]
        public void TestFindMaxAbs()
        {
            float[] numbers = { 5, -6.1f, 0, -8, 0.1f, 7 };
            Task3 t = new Task3();
            Assert.AreEqual(8, t.FindMaxAbs(numbers));
        }

        [TestMethod]
        public void TestSumBetweenFirstTwoPositive()
        {
            float[] numbers = { 5.1f, -1, -3, 0, 7 };

            Task3 t = new Task3();

            Assert.AreEqual(-4, t.SumBetweenFirstTwoPositive(numbers));
        }

        [TestMethod]
        public void TestMoveZeroToEnd()
        {
            float[] numbers = { 5.1f, 0, -3, 0, 7 };
            float[] expected = { 5.1f, -3, 7, 0, 0 };

            Task3 t = new Task3();

            CollectionAssert.AreEqual(expected, t.MoveZeroToEnd(numbers));
        }
    }
}
