﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab2;

namespace LabsHNEU.Tests.lab2
{
    [TestClass]
    public class WordEndingsTests
    {
        [TestMethod]
        public void TestSimpleEnding()
        {
            WordEndingsFilter wordEndingsFilter = new WordEndingsFilter();

            string[] endings = { "ing", "ed" };
            string input = "I am working on oop labs. I have tested labs by unit tests.";
            string expected = "I am on oop labs. I have labs by unit tests.";

            Assert.AreEqual(expected, wordEndingsFilter.filter(input, endings));
        }

        [TestMethod]
        public void TestFirterFirstWord()
        {
            WordEndingsFilter wordEndingsFilter = new WordEndingsFilter();

            string[] endings = { "ать" };
            string input = "Работать над, лабами. никогда? не!... поздно";
            string expected = "Над, лабами. никогда? не!... поздно";

            Assert.AreEqual(expected, wordEndingsFilter.filter(input, endings));
        }
    }
}
