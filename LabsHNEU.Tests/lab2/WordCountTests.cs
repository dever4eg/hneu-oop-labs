﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab2;

namespace LabsHNEU.Tests.lab2
{
    [TestClass]
    public class WordCountTests
    {
        [TestMethod]
        public void TestWordCountFilter()
        {
            WordCountFilter wordCountFilter = new WordCountFilter();

            int wordCount = 6;
            string input = "I am working on oop labs. I have tested labs by unit tests.";
            string expected = "I am working on oop labs.";

            Assert.AreEqual(expected, wordCountFilter.Filter(input, wordCount));
        }
    }
}
