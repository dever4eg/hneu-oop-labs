﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabsHNEU.Lab2;

namespace LabsHNEU.Tests.lab2
{
    [TestClass]
    public class ExpressionAnalyzerTests
    {
        [TestMethod]
        public void TestSimpleNumber()
        {
            ConstExpression expression = new ConstExpression();
            string input = "12432";
            Assert.AreEqual(12432, expression.Parse(input));
        }

        [TestMethod]
        public void TestSimpleOperation()
        {
            ConstExpression expression = new ConstExpression();
            string input = "23+5";
            Assert.AreEqual(28, expression.Parse(input));
        }

        [TestMethod]
        public void TestRecursiveFirstExpression()
        {
            ConstExpression expression = new ConstExpression();
            string input = "(23+5)+4";
            Assert.AreEqual(32, expression.Parse(input));
        }

        [TestMethod]
        public void TestRecursiveTwoExpressions()
        {
            ConstExpression expression = new ConstExpression();
            string input = "(23+5)+(4-2)";
            Assert.AreEqual(30, expression.Parse(input));
        }

        [TestMethod]
        public void TestRecursiveTwoLevel()
        {
            ConstExpression expression = new ConstExpression();
            string input = "(23+5)+(4-(1*3))";
            Assert.AreEqual(29, expression.Parse(input));
        }

        [TestMethod]
        public void TestSpaces()
        {
            ConstExpression expression = new ConstExpression();
            string input = " ( 23 + 5 ) + ( 4 - ( 1 * 3 ) ) ";
            Assert.AreEqual(29, expression.Parse(input));
        }
    }
}
